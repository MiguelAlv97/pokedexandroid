package com.example.sebastian.pokedex

import android.content.Intent
import android.nfc.Tag
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokedexActivity : AppCompatActivity(), PokedexAdapter.PokedexSelectionListener{


    companion object {
    val INTENT_POKEMON_NAME = "pokemonName"
  }

  lateinit var linearLayoutManager: LinearLayoutManager
  lateinit var pokedexRecyclerView: RecyclerView
  lateinit var scrollListener: RecyclerView.OnScrollListener
  private val lastVisiblePosition: Int
    get() = linearLayoutManager.findLastVisibleItemPosition()


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_pokedex)

    linearLayoutManager = LinearLayoutManager(this)

    pokedexRecyclerView = findViewById(R.id.pokedex_recycler_view)
    pokedexRecyclerView.layoutManager = LinearLayoutManager(this)
    pokedexRecyclerView.adapter = PokedexAdapter(this)

  }

  fun showSinglePokemon(pokemonName:String) {
    val singlePokemonIntent = Intent(this, SinglePokemonActivity::class.java)
    singlePokemonIntent.putExtra(INTENT_POKEMON_NAME, pokemonName)
    startActivity(singlePokemonIntent)
  }

  override fun pokemonSelected(pokemonName: String) {
    showSinglePokemon(pokemonName)
  }

  private fun setViewScrollListener(){
    scrollListener = object : RecyclerView.OnScrollListener(){
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        val totalItemCount = recyclerView.layoutManager!!.itemCount
        if(totalItemCount == lastVisiblePosition + 1){
          //aqui va la magia para descargar
            val adapter = pokedexRecyclerView.adapter as PokedexAdapter
            adapter.loadPokemon()
            pokedexRecyclerView.removeOnScrollListener(scrollListener)

        }
      }
    }
      pokedexRecyclerView.addOnScrollListener(scrollListener)
  }
    override fun fetchCompleated() {

        setViewScrollListener()
    }
}
