package com.example.sebastian.pokedex

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {

  @GET("/api/v2/pokemon")
  fun getPokemon(@Query("offset") offset:Int = 0,
                 @Query("limit") limit:Int = 20)
          : Call<PokedexResponse>

  @GET("/api/v2/pokemon/{pokemon_name}")
  fun getSinglePokemon(@Path("pokemon_name") name: String)
          :Call<PokedexResponse.PokemonResponse>

  companion object {
    val instance: PokemonService by lazy {
      val retrofit = Retrofit.Builder()
              .baseUrl("https://pokeapi.co/")
              .addConverterFactory(GsonConverterFactory.create())
              .build()
      retrofit.create<PokemonService>(PokemonService::class.java)
    }
  }
}