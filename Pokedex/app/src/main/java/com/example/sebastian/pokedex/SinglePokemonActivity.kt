package com.example.sebastian.pokedex

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SinglePokemonActivity : AppCompatActivity() {

  lateinit var pokemonName: String
  lateinit var pokemonHeight: TextView
  lateinit var pokemonWeight: TextView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_single_pokemon)
    pokemonHeight = findViewById(R.id.pokemon_height)
    pokemonWeight = findViewById(R.id.pokemon_weight)

    pokemonName = intent.getStringExtra(PokedexActivity.INTENT_POKEMON_NAME)
    title = pokemonName

    val pokemonService = PokemonService.instance
    val singlePokemonCall = pokemonService.getSinglePokemon(pokemonName)

    singlePokemonCall.enqueue(object : Callback<PokedexResponse.PokemonResponse> {
      override fun onFailure(call: Call<PokedexResponse.PokemonResponse>?, t: Throwable?) {
        if (call != null) {
        }
      }

      override fun onResponse(call: Call<PokedexResponse.PokemonResponse>?, response: Response<PokedexResponse.PokemonResponse>?) {
        if (response != null) {
          val body = response.body() as PokedexResponse.PokemonResponse
          pokemonHeight.text = getString(R.string.height, body.height)
          pokemonWeight.text = getString(R.string.weight, body.weight)
        }
      }
    })

  }
}
